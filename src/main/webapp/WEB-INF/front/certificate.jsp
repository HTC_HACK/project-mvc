<%--
  Created by IntelliJ IDEA.
  User: koh
  Date: 3/1/22
  Time: 2:52 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <title>Certificate</title>
    <link href="https://fonts.googleapis.com/css?family=Satisfy" rel="stylesheet">
    <style>


        .cert {
            border: 15px solid #0072c6;
            border-right: 15px solid #0894fb;
            border-left: 15px solid #0894fb;
            width: 700px;
            font-family: arial;
            color: #383737;
        }

        .crt_title {
            margin-top: 30px;
            font-family: "Satisfy", cursive;
            font-size: 40px;
            letter-spacing: 1px;
            color: #0060a9;
        }

        .crt_logo img {
            width: 130px;
            height: auto;
            margin: auto;
            padding: 30px;
        }

        .colorGreen {
            color: #27ae60;
        }

        .crt_user {
            display: inline-block;
            width: 80%;
            padding: 5px 25px;
            margin-bottom: 0px;
            padding-bottom: 0px;
            font-family: "Satisfy", cursive;
            font-size: 40px;
            border-bottom: 1px dashed #cecece;
        }

        .afterName {
            font-weight: 100;
            color: #383737;
        }

        .colorGrey {
            color: grey;
        }

        .certSign {
            width: 200px;
        }

        @media (max-width: 700px) {
            .cert {
                width: 100%;
            }
        }
    </style>
</head>

<body>
<div class="container" style="text-align: center;padding: 3rem 18rem">
    <table class="cert">
        <tr>
            <td align="center">
                <h1 class="crt_title">${course} Certificate Of Completion
                    <h2>This Certificate is awarded to</h2>
                    <h1 class="colorGreen crt_user">${authUser.userDetail.firstName} ${authUser.userDetail.lastName}</h1>
                    <h3 class="afterName">For his completion of Team Inc Awareness session
                    </h3>
                    <h3>Awarded on 2 March 2022 </h3>
            </td>
        </tr>
        <tr>
            <td align="center">
                <h3>Person Name Asadbek Xalimjonov</h3>
                <h3>Department Name Team Inc</h3>
            </td>
        </tr>
    </table>
</div>
<script>
</script>
</body>
</html>