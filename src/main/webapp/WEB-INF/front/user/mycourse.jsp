<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>My Courses</title>
    <%@ include file="../layouts/link.html" %>
    <style>
        .progress {
            margin: 20px 1rem 20px auto;
            padding: 0;

            width: 90%;
            height: 30px;
            overflow: hidden;
            background: #e5e5e5;
            border-radius: 6px;
        }

        .bar {
            position: relative;
            float: left;
            min-width: 1%;
            height: 100%;
            background: #FD661F;
        }

        .percent {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            margin: 0;
            font-family: tahoma, arial, helvetica;
            font-size: 12px;
            color: white;
        }

        .progress1 {
            margin: 20px 1rem 20px auto;
            padding: 0;

            width: 90%;
            height: 30px;
            overflow: hidden;
            background: #e5e5e5;
            border-radius: 6px;
        }

        .bar1 {
            position: relative;
            float: left;
            min-width: 1%;
            height: 100%;
            background: #00938D;
        }

        .percent1 {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            margin: 0;
            font-family: tahoma, arial, helvetica;
            font-size: 12px;
            color: white;
        }
    </style>
</head>
<body>
<%@ include file="../layouts/menu.jsp" %>


<section class="category-section">
    <!-- Container -->
    <!--/ Container - -->
</section>
<!--   End: Popular Categories Section
==================================================-->


<!-- Start: Featured Courses Section
==================================================-->
<section class="feat-course-section">
    <div class="container">
        <!-- Start: Heading -->
        <div class="base-header" style="margin-bottom: 2rem">
            <h3> My Courses </h3>
        </div>
        <!-- End: Heading -->
        <div class="row">

            <c:forEach var="course" items="${courses}">

                <div class="col-lg-4 col-md-6 col-sm-12">
                    <a href="/courses/detail/${course.id}">
                        <div class="feat_course_item">
                            <a href="/courses/detail/${course.id}"> <img
                                    src="data:image/png;base64,${course.image_path}" alt="image" style="height: 188.55px"></a>
                            <div class="feat_cour_price">
                                <span class="feat_cour_tag"> ${course.name} </span>
                                <span class="feat_cour_p"> <c:if test="${course.price>0}">$${course.price}</c:if><c:if
                                        test="${course.price<1}">Free</c:if> </span>
                            </div>
                            <a href="/courses/detail/${course.id}"><h4
                                    class="feat_cour_tit"> ${course.description} </h4></a>
                            <div class="feat_cour_rating">
                            <span class="feat_cour_rat">
                            </span>
                                <c:if test="${course.percent.equals('%')}">
                                    <div class="progress1">
                                        <div class="bar1" style="width:100%">
                                            <p class="percent1">0%</p>
                                        </div>
                                    </div>
                                </c:if>
                                <c:if test="${!course.percent.equals('%')}">
                                    <div class="progress">
                                        <div class="bar" style="width:${course.percent}">
                                            <p class="percent">${course.percent}</p>
                                        </div>
                                    </div>
                                </c:if>
                                <a href="/courses/detail/${course.id}"> <i class="arrow_right"></i> </a>
                            </div>
                        </div>
                    </a>
                </div>
            </c:forEach>
            <!-- /. col-lg-4 col-md-6 col-sm-12-->

            <!-- /. col-lg-4 col-md-6 col-sm-12-->
        </div>
        <!-- /. row -->
    </div>
    <!-- /. container -->
</section>

<%@ include file="../layouts/footer.html" %>
<%@ include file="../layouts/scripts.html" %>
</body>
</html>
