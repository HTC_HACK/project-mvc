<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: koh
  Date: 2/28/22
  Time: 8:16 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Online Quiz</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>

<body>

<c:if test="${tests.size()>0}">
    <div class="container" style="padding:3rem;">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <form method="post" action="/courses/start/test/${courseId}"
                      style="border:2px solid #3dcfd3;padding:1rem;border-radius:10px">
                    <c:forEach var="data" items="${tests}">
                        <div class="form-group">
                            <input type="hidden" name="${data.id}" value="${data.id}">
                            <label>${data.question}</label>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="${data.id}" id="inlineRadio1"
                                       value="1">
                                <label class="form-check-label" for="inlineRadio1">
                                        ${data.option_one}
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="${data.id}" id="inlineRadio2"
                                       value="2">
                                <label class="form-check-label" for="inlineRadio2">
                                        ${data.option_two}
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="${data.id}" id="inlineRadio3"
                                       value="3">
                                <label class="form-check-label" for="inlineRadio3">
                                        ${data.option_three}
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="${data.id}" id="inlineRadio4"
                                       value="4">
                                <label class="form-check-label" for="inlineRadio4">
                                        ${data.option_four}
                                </label>
                            </div>
                            <br>
                        </div>
                    </c:forEach>
                    <div class="form-group">
                        <button type="submit" class="btn btn-info" name="subjects">Submit</button>
                    </div>
                </form>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
</c:if>
<c:if test="${tests.size()<1}">
    <h1>No Tests Yet</h1>
</c:if>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>

</html>
