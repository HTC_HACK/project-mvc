<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: koh
  Date: 2/18/22
  Time: 2:14 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <%@ include file="store/link.jsp" %>
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
    <!-- Preloader -->
    <%@ include file="store/menu.jsp" %>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Dashboard</h1>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>

        <!-- /.content-header -->
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
                <div class="row">
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-info">
                            <div class="inner">
                                <h3>${usersCount}</h3>
                                <p>Users</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-users"></i>
                            </div>
                            <a href="/admin/users" class="small-box-footer">More info <i
                                    class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <c:if test="${user.role.id==1}">
                        <div class="col-lg-3 col-6">
                            <div class="small-box bg-primary">
                                <div class="inner">
                                    <h3>${mentorCount}</h3>
                                    <p>Mentors</p>
                                </div>
                                <div class="icon">
                                    <i class="fas fa-user-alt-slash"></i>
                                </div>
                                <a href="/admin/users" class="small-box-footer">More info <i
                                        class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-6">
                            <div class="small-box bg-info">
                                <div class="inner">
                                    <h3>${studentCount}</h3>
                                    <p>Students</p>
                                </div>
                                <div class="icon">
                                    <i class="fas fa-user-secret"></i>
                                </div>
                                <a href="/admin/users" class="small-box-footer">More info <i
                                        class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                    </c:if>

                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <c:if test="${user.role.id==1}">
                            <div class="small-box bg-success">
                                <div class="inner">
                                    <h3>${coursesCount}</h3>
                                    <p>Courses</p>
                                </div>
                                <div class="icon">
                                    <i class="fas fa-laptop-code"></i>
                                </div>
                                <a href="/courses" class="small-box-footer">More info <i
                                        class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </c:if>
                        <c:if test="${user.role.id==2}">
                            <div class="small-box bg-success">
                                <div class="inner">
                                    <h3>${coursesCount}</h3>
                                    <p>Courses</p>
                                </div>
                                <div class="icon">
                                    <i class="fas fa-laptop-code"></i>
                                </div>
                                <a href="/courses" class="small-box-footer">More info <i
                                        class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </c:if>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <c:if test="${user.role.id==1}">
                            <div class="small-box bg-warning">
                                <div class="inner">
                                    <h3>${modelsCount}</h3>
                                    <p>Modules</p>
                                </div>
                                <div class="icon">
                                    <i class="fas fa-chalkboard"></i>
                                </div>
                                <a href="/modules" class="small-box-footer">More info <i
                                        class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </c:if>
                        <c:if test="${user.role.id==2}">
                            <div class="small-box bg-warning">
                                <div class="inner">
                                    <h3>${modelsCount}</h3>
                                    <p>Modules</p>
                                </div>
                                <div class="icon">
                                    <i class="fas fa-chalkboard"></i>
                                </div>
                                <a href="/modules" class="small-box-footer">More info <i
                                        class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </c:if>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-6">
                        <c:if test="${user.role.id==1}">
                            <!-- small box -->
                            <div class="small-box bg-danger">
                                <div class="inner">
                                    <h3>${lesonsCount}</h3>
                                    <p>Lessons</p>
                                </div>
                                <div class="icon">
                                    <i class="fas fa-book"></i>
                                </div>
                                <a href="/lessons" class="small-box-footer">More info <i
                                        class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </c:if>
                        <c:if test="${user.role.id==2}">
                            <!-- small box -->
                            <div class="small-box bg-danger">
                                <div class="inner">
                                    <h3>${lesonsCount}</h3>
                                    <p>Lessons</p>
                                </div>
                                <div class="icon">
                                    <i class="fas fa-book"></i>
                                </div>
                                <a href="/lessons" class="small-box-footer">More info <i
                                        class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </c:if>
                    </div>
                    <!-- ./col -->
                </div>
                <!-- /.row -->
                <!-- Main row -->
                <!-- /.row (main row) -->
            </div><!-- /.container-fluid -->

            <div class="row">
                <!-- Left col -->
                <div class="col-md-6">
                    <!-- /.card -->

                    <!-- Donut chart -->
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <h3 class="card-title">
                                <i class="far fa-chart-bar"></i>
                                Student Enrollment
                            </h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="piechart"></div>
                        </div>
                        <!-- /.card-body-->
                    </div>
                    <!-- /.card -->
                </div>
                <c:if test="${authUser.role.id==1}">
                    <div class="col-md-6">
                        <!-- /.card -->

                        <!-- Donut chart -->
                        <div class="card card-primary card-outline">
                            <div class="card-header">
                                <h3 class="card-title">
                                    <i class="far fa-chart-bar"></i>
                                    Users Stats
                                </h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <div id="piechart2"></div>
                            </div>
                            <!-- /.card-body-->
                        </div>
                        <!-- /.card -->
                    </div>
                </c:if>
                <c:if test="${authUser.role.id==2}">
                    <div class="col-md-6">
                        <!-- /.card -->

                        <!-- Donut chart -->
                        <div class="card card-primary card-outline">
                            <div class="card-header">
                                <h3 class="card-title">
                                    <i class="far fa-chart-bar"></i>
                                    Students Enrollment Stats By your Course
                                </h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <div id="piechart3"></div>
                            </div>
                            <!-- /.card-body-->
                        </div>
                        <!-- /.card -->
                    </div>
                </c:if>

            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <strong>Copyright &copy; 2014-2021 <a href="https://adminlte.io">AdminLTE.io</a>.</strong>
        All rights reserved.
        <div class="float-right d-none d-sm-inline-block">
            <b>Version</b> 3.2.0
        </div>
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
<%@ include file="store/script.html" %>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
    // Load google charts

    const handleHttpRequest = async (url) => {
        try {
            const response = await fetch(url);

            if (!response.ok) {
                throw new Error(response.message)
            }

            const responseData = await response.json();

            const courseArray = responseData.map((el) => {
                return [el[0], el[1]]
            });

            console.log("success", courseArray)

            google.charts.load('current', {'packages':['corechart']});
            google.charts.setOnLoadCallback(() => {
                drawChart(courseArray)
            });

            // for (let i of responseData) {
            //     console.log(i)
            // }

        } catch (error) {
            console.log("error", error)
        }
    }

    // Draw the chart and set the chart values
    function drawChart(arr) {

        console.log("arr", arr)

        var data = google.visualization.arrayToDataTable(arr);

        // Optional; add a title and set the width and height of the chart
        var options = {'width':485, 'height':400};

        // Display the chart inside the <div> element with id="piechart"
        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
        chart.draw(data, options);
    }

    handleHttpRequest("http://localhost:8080/stats")


    const handleHttpRequest2 = async (url) => {
        try {
            const response = await fetch(url);

            if (!response.ok) {
                throw new Error(response.message)
            }

            const responseData = await response.json();

            const courseArray = responseData.map((el) => {
                return [el[0], el[1]]
            });

            console.log("success", courseArray)

            google.charts.load('current', {'packages':['corechart']});
            google.charts.setOnLoadCallback(() => {
                drawChart2(courseArray)
            });

            // for (let i of responseData) {
            //     console.log(i)
            // }

        } catch (error) {
            console.log("error", error)
        }
    }

    // Draw the chart and set the chart values
    function drawChart2(arr) {

        console.log("arr", arr)

        var data = google.visualization.arrayToDataTable(arr);

        // Optional; add a title and set the width and height of the chart
        var options = {'width':485, 'height':400};

        // Display the chart inside the <div> element with id="piechart"
        var chart = new google.visualization.PieChart(document.getElementById('piechart2'));
        chart.draw(data, options);
    }

    handleHttpRequest2("http://localhost:8080/stats/user")


    const handleHttpRequest3 = async (url) => {
        try {
            const response = await fetch(url);

            if (!response.ok) {
                throw new Error(response.message)
            }

            const responseData = await response.json();

            const courseArray = responseData.map((el) => {
                return [el[0], el[1]]
            });

            console.log("success", courseArray)

            google.charts.load('current', {'packages':['corechart']});
            google.charts.setOnLoadCallback(() => {
                drawChart3(courseArray)
            });

            // for (let i of responseData) {
            //     console.log(i)
            // }

        } catch (error) {
            console.log("error", error)
        }
    }

    // Draw the chart and set the chart values
    function drawChart3(arr) {

        console.log("arr", arr)

        var data = google.visualization.arrayToDataTable(arr);

        // Optional; add a title and set the width and height of the chart
        var options = {'width':485, 'height':400};

        // Display the chart inside the <div> element with id="piechart"
        var chart = new google.visualization.PieChart(document.getElementById('piechart3'));
        chart.draw(data, options);
    }

    handleHttpRequest3("http://localhost:8080/stats/user/"+${authUser.id})




    <%--$(function () {--%>

    <%--    fetch('http://localhost:8080/stats/user/'+${authUser.id})--%>
    <%--        .then((response) => {--%>
    <%--            return response.json();--%>
    <%--        })--%>
    <%--        .then((data) => {--%>
    <%--            $.plot('#donut-chart3', data, {--%>
    <%--                series: {--%>
    <%--                    pie: {--%>
    <%--                        show: true,--%>
    <%--                        radius: 1,--%>
    <%--                        innerRadius: 0.5,--%>
    <%--                        label: {--%>
    <%--                            show: true,--%>
    <%--                            radius: 2 / 3,--%>
    <%--                            formatter: labelFormatter,--%>
    <%--                            threshold: 0.1--%>
    <%--                        }--%>

    <%--                    }--%>
    <%--                },--%>
    <%--                legend: {--%>
    <%--                    show: false--%>
    <%--                }--%>
    <%--            })--%>
    <%--        });--%>

    <%--    fetch('http://localhost:8080/stats/user')--%>
    <%--        .then((response) => {--%>
    <%--            return response.json();--%>
    <%--        })--%>
    <%--        .then((data) => {--%>
    <%--            $.plot('#donut-chart2', data, {--%>
    <%--                series: {--%>
    <%--                    pie: {--%>
    <%--                        show: true,--%>
    <%--                        radius: 1,--%>
    <%--                        innerRadius: 0.5,--%>
    <%--                        label: {--%>
    <%--                            show: true,--%>
    <%--                            radius: 2 / 3,--%>
    <%--                            formatter: labelFormatter,--%>
    <%--                            threshold: 0.1--%>
    <%--                        }--%>

    <%--                    }--%>
    <%--                },--%>
    <%--                legend: {--%>
    <%--                    show: false--%>
    <%--                }--%>
    <%--            })--%>
    <%--        });--%>


    <%--})--%>

    <%--$(function () {--%>

    <%--    fetch('http://localhost:8080/stats')--%>
    <%--        .then((response) => {--%>
    <%--            return response.json();--%>
    <%--        })--%>
    <%--        .then((data) => {--%>
    <%--            $.plot('#donut-chart', data, {--%>
    <%--                series: {--%>
    <%--                    pie: {--%>
    <%--                        show: true,--%>
    <%--                        radius: 1,--%>
    <%--                        innerRadius: 0.5,--%>
    <%--                        label: {--%>
    <%--                            show: true,--%>
    <%--                            radius: 2 / 3,--%>
    <%--                            formatter: labelFormatter,--%>
    <%--                            threshold: 0.1--%>
    <%--                        }--%>

    <%--                    }--%>
    <%--                },--%>
    <%--                legend: {--%>
    <%--                    show: false--%>
    <%--                }--%>
    <%--            })--%>
    <%--        });--%>


    <%--})--%>

    <%--function labelFormatter(label, series) {--%>
    <%--    return '<div style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">'--%>
    <%--        + label--%>
    <%--        + '<br>'--%>
    <%--        + Math.round(series.percent) + '%</div>'--%>
    <%--}--%>

</script>
</body>
</html>