<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: koh
  Date: 2/18/22
  Time: 2:14 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <%@ include file="store/link.jsp" %>
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

    <!-- Preloader -->


    <%@ include file="store/menu.jsp" %>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Results</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example2" class="table table-bordered table-hover"
                                       style="text-align: center">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>FullName</th>
                                        <th>Course</th>
                                        <th>Result</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:if test="${results.size()>0}">
                                    <c:forEach var="result" items="${results}" varStatus="loop">
                                    <tr>
                                        <td>${loop.count}</td>
                                        <td>${result.fullName}
                                        </td>
                                        <td>${result.course}</td>
                                        <c:if test="${result.score>=60 && result.score<=80}">
                                            <td>
                                                <button class="btn btn-warning">${result.score}%</button>
                                            </td>
                                        </c:if>
                                        <c:if test="${result.score>80}">
                                            <td>
                                                <button class="btn btn-success">${result.score}%</button>
                                            </td>
                                        </c:if>
                                        <c:if test="${result.score<60}">
                                            <td>
                                                <button class="btn btn-danger">${result.score}%</button>
                                            </td>
                                        </c:if>
                                    </tr>
                                    </c:forEach>
                                    </c:if>
                                    <c:if test="${results.size()<1}">
                                    <tr>
                                        <td colspan="4">No Data</td>
                                    </tr>
                                    </c:if>
                                    </tfoot>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <strong>Copyright &copy; 2014-2021 <a href="https://adminlte.io">AdminLTE.io</a>.</strong>
        All rights reserved.
        <div class="float-right d-none d-sm-inline-block">
            <b>Version</b> 3.2.0
        </div>
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
<%@ include file="store/script.html" %>
</body>
</html>

