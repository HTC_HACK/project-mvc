<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Courses</title>
    <%@ include file="layouts/link.html" %>

</head>
<body>
<%@ include file="layouts/menu.jsp" %>
<!-- Start: Hero Section
==================================================-->
<!-- End: Hero Section
==================================================-->
<!-- Start: Popular Categories Section
==================================================-->
<section class="category-section">
    <!-- Container -->
    <!--/ Container - -->
</section>
<!--   End: Popular Categories Section
==================================================-->


<!-- Start: Featured Courses Section
==================================================-->
<section class="course_cat_section" style="margin-top: -6rem">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-3 course_sidebar">
                <!-- end: Category Widget-->

                <div class="course_price_sidebar">
                    <h4 class="course_cat_title">Price Level</h4>
                    <ul>
                        <li>
                            <div class="course_cat_check">
                                <input class="cat-check-input" type="radio" value="" id="priceCheck1" checked=""
                                       name="priceCheck">
                                <label class="cat-check-label" for="priceCheck1">
                                    Free
                                </label>
                            </div>
                        </li>
                        <li>
                            <div class="course_cat_check">
                                <input class="cat-check-input" type="radio" value="" id="priceCheck2" checked=""
                                       name="priceCheck">
                                <label class="cat-check-label" for="priceCheck2">
                                    Paid
                                </label>
                            </div>
                        </li>

                    </ul>
                </div>
                <!-- end: Price Level Widget-->

                <div class="course_instructor_sidebar">
                    <h4 class="course_cat_title">Instructor </h4>
                    <ul>
                        <c:forEach var="instructor" items="${instructors}">
                            <li>
                                <div class="course_instructor_check">
                                    <input class="instructor-check-input" type="checkbox" value=""
                                           id="instructorCheck${instructor.id}"
                                           name="instructor">
                                    <label class="instructor-check-label" for="instructorCheck${instructor.id}">
                                            ${instructor.firstname} ${instructor.lastname}
                                    </label>
                                </div>
                            </li>
                        </c:forEach>
                    </ul>
                </div>
            </div>
            <!-- end: col-sm-12 col-lg-3-->

            <!--  Start: col-sm-12 col-lg-9 -->
            <div class="col-sm-12 col-lg-9">
                <div class="row">
                    <div class="col-sm-12 cat_search_filter">
                        <div class="cat_search">
                            <div class="widget widget-search">
                                <!-- input-group -->
                                <form method="GET" action="/courseCategory">
                                    <div class="input-group">
                                        <input class="form-control" name="search" placeholder="Search" type="text">
                                        <span class="input-group-btn">
                                            <button type="button"><i class="fas fa-search"></i></button>
                                        </span>
                                    </div>
                                </form>
                                <!-- /input-group -->
                            </div>
                        </div>
                        <!-- End: Search -->

                        <!-- end: select box -->
                        <div class="cat_item_count">
                            Showing ${courses.size()} of ${size} results
                        </div>
                    </div>
                    <!--  End: Search Filter -->
                    <c:if test="${courses.size()>0}">
                        <c:forEach var="course" items="${courses}">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <a href="/courses/detail/${course.id}">
                                    <div class="feat_course_item">
                                        <a href="/courses/detail/${course.id}"> <img
                                                src="data:image/png;base64,${course.image_path}" alt="image"
                                                style="height: 200px    "></a>
                                        <div class="feat_cour_price">
                                            <span class="feat_cour_tag"> ${course.name} </span>
                                            <span class="feat_cour_p"> <c:if
                                                    test="${course.price>0}">$${course.price}</c:if><c:if
                                                    test="${course.price<1}">Free</c:if> </span>
                                        </div>
                                        <a href="/courses/detail/${course.id}"><h4
                                                class="feat_cour_tit"> ${course.description} </h4></a>
                                        <div class="feat_cour_lesson">
                                        <span class="feat_cour_less"> <i
                                                class="fas fa-sticky-note"></i> ${course.lesson_count} lessons </span>
                                            <span class="feat_cour_stu"> <i
                                                    class="fas fa-user"></i> ${course.student_count} Students </span>
                                        </div>
                                        <div class="feat_cour_rating">
                            <span class="feat_cour_rat">
                            </span>
                                            <a href="/courses/detail/${course.id}"> <i class="arrow_right"></i> </a>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </c:forEach>
                    </c:if>
                    <c:if test="${courses.size()<1}">
                        <p style="padding-left:1rem;font-size: 20px">No Data Found</p>
                    </c:if>
                    <!-- /. col-lg-4 col-md-6 col-sm-12-->

                </div>

                <nav class="cat-page-navigation">
                    <ul class="pagination">

                        <%
                            int size = (int) request.getAttribute("size");
                            int active = (int) request.getAttribute("pageid");
                            int floor = (size / 2);
                            int h = size % 2;
                            if (h > 0) {
                                floor += 1;
                            }

                            int floor2 = 1;
                            boolean isFlag = false;
                            for (int j = 1; j <= floor; j++) {
                                if (isFlag == false && active == floor2) {
                                    isFlag = true;
                                    out.print("<li><a class=\"active\" href=\"/courseCategory?pageid=" + floor2 + "\">" + floor2 + "</a></li>");
                                } else {
                                    out.print("<li><a href=\"/courseCategory?pageid=" + floor2 + "\">" + floor2 + "</a></li>");
                                }
                                floor2 += 1;
                            }
                        %>

                    </ul>
                </nav>
                <!-- end:  pagination-->
            </div>
        </div>
        <!-- /. row -->
    </div>
    <!-- /. container -->
</section>
<!-- End: Featured Courses Section
==================================================-->


<!-- End: Newsletter Section
==================================================-->

<%@ include file="layouts/footer.html" %>
<%@ include file="layouts/scripts.html" %>
</body>
</html>
