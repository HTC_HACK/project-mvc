<%--
  Created by IntelliJ IDEA.
  User: koh
  Date: 3/2/22
  Time: 1:47 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Certificate ${authUser.email}</title>
    <link rel="stylesheet" href="/css/certificate.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.min.js"></script>
</head>
<body>
<div class="toolbar no-print" style="background-color: #fff">
    <a style="text-decoration: none" href="/" class="btn btn-success">
        Home
    </a>
    <button class="btn btn-info" onclick="window.print()">
        Print Certificate
    </button>
    <button class="btn btn-info" id="downloadPDF">Download PDF</button>
</div>
<div class="cert-container print-m-0">
    <div id="content2" class="cert">
        <img
                src="https://edgarsrepo.github.io/certificate.png"
                class="cert-bg"
                alt=""
        />
        <div class="cert-content">
            <h1 class="other-font">Certificate of Completion</h1>
            <span style="font-size: 30px;">${course}</span>
            <br/><br/><br/><br/>
            <span class="other-font"
            ><i><b>has completed the</b></i></span
            >
            <br/>
            <span style="font-size: 40px;"><b>${authUser.userDetail.firstName} ${authUser.userDetail.lastName}</b></span>
            <br/>
            <small></small>
            <br/><br/><br/><br/>
            <div class="bottom-txt">
                <span>TEAM-DEVS GROUP INC</span>
                <span>Completed on: ${issed_on}</span>
            </div>
        </div>
    </div>
</div>
<script>
    $("#downloadPDF").click(function () {
        // $("#content2").addClass('ml-215'); // JS solution for smaller screen but better to add media queries to tackle the issue
        getScreenshotOfElement(
            $("div#content2").get(0),
            0,
            0,
            $("#content2").width() + 45,  // added 45 because the container's (content2) width is smaller than the image, if it's not added then the content from right side will get cut off
            $("#content2").height() + 30, // same issue as above. if the container width / height is changed (currently they are fixed) then these values might need to be changed as well.
            function (data) {
                var pdf = new jsPDF("l", "pt", [
                    $("#content2").width(),
                    $("#content2").height(),
                ]);

                pdf.addImage(
                    "data:image/png;base64," + data,
                    "PNG",
                    0,
                    0,
                    $("#content2").width(),
                    $("#content2").height()
                );
                pdf.save("certificate.pdf");
            }
        );
    });

    // this function is the configuration of the html2cavas library (https://html2canvas.hertzen.com/)
    // $("#content2").removeClass('ml-215'); is the only custom line here, the rest comes from the library.
    function getScreenshotOfElement(element, posX, posY, width, height, callback) {
        html2canvas(element, {
            onrendered: function (canvas) {
                // $("#content2").removeClass('ml-215');  // uncomment this if resorting to ml-125 to resolve the issue
                var context = canvas.getContext("2d");
                var imageData = context.getImageData(posX, posY, width, height).data;
                var outputCanvas = document.createElement("canvas");
                var outputContext = outputCanvas.getContext("2d");
                outputCanvas.width = width;
                outputCanvas.height = height;

                var idata = outputContext.createImageData(width, height);
                idata.data.set(imageData);
                outputContext.putImageData(idata, 0, 0);
                callback(outputCanvas.toDataURL().replace("data:image/png;base64,", ""));
            },
            width: width,
            height: height,
            useCORS: true,
            taintTest: false,
            allowTaint: false,
        });
    }

</script>

</body>
</html>
