package uz.pdp.dtos;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.model.User;

@AllArgsConstructor
@NoArgsConstructor
@Data

//Asadbek Xalimjonov 2/26/22 1:17 PM

public class UserAnswerDto {

    private Integer id;
    private String answer;
    private String image_path;

    private Boolean status;
    private Boolean correct;
    private String taskDescription;
    private String fullName;
    private String lessonName;
}
