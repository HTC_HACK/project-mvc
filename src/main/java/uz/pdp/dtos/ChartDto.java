package uz.pdp.dtos;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data

//Asadbek Xalimjonov 2/26/22 9:19 PM

public class ChartDto {

    private String label;
    private Double data;
}
