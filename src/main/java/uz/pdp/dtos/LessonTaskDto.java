package uz.pdp.dtos;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data

//Asadbek Xalimjonov 2/26/22 1:02 PM

public class LessonTaskDto {


    private Integer id;
    private String description;
    private String file_path;
    private boolean isCorrect;
    private Integer lessonId;
    private List<UserAnswerDto> userAnswers = new ArrayList<>();


}
