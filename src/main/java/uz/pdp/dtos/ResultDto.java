package uz.pdp.dtos;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data

//Asadbek Xalimjonov 2/28/22 10:11 PM

public class ResultDto {

    private Integer id;
    private String fullName;
    private Double score;
    private String course;

}
