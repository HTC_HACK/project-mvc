package uz.pdp.service;



//Asadbek Xalimjonov 2/26/22 12:15 PM


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.pdp.dao.LessonTaskDao;
import uz.pdp.dao.TaskDao;
import uz.pdp.dtos.LessonTaskDto;
import uz.pdp.model.LessonTask;

import java.util.List;

@Service
@Transactional
public class LessonTaskService {

    @Autowired
    LessonTaskDao lessonTaskDao;

    @Autowired
    TaskDao taskDao;

    public List<LessonTaskDto> getTaskByLessonId(Integer lessonId,Integer userId)
    {
        return lessonTaskDao.getTaskByLessonId(lessonId,userId);
    }

    public List<LessonTaskDto> getLessonTask(Integer lesonId)
    {
        return taskDao.getLessonTaskDtoByLessonId(lesonId);
    }

}
