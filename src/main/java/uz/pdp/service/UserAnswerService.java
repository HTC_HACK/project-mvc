package uz.pdp.service;


//Asadbek Xalimjonov 2/26/22 12:46 PM


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.pdp.dao.UserAnswerDao;
import uz.pdp.dtos.UserAnswerDto;

import java.util.List;

@Service
@Transactional
public class UserAnswerService {


    @Autowired
    UserAnswerDao userAnswerDao;

    public void saveAnswer(Integer userId, Integer taskId, String answer) {
        userAnswerDao.saveAnswer(userId, taskId, answer);
    }

    public List<UserAnswerDto> getUnCheckedAnswer(Integer id) {
        return userAnswerDao.getUnCheckedAnswer(id);
    }

    public List<UserAnswerDto> getCheckedAnswer(Integer id) {
        return userAnswerDao.getCheckedAnswer(id);
    }

    public void makeAnswerTrue(Integer answerId) {
        userAnswerDao.makeAnswerTrue(answerId);
    }

    public void makeAnswerFalse(Integer answerId) {
        userAnswerDao.makeAnswerFalse(answerId);
    }
}
