package uz.pdp.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Data

//Asadbek Xalimjonov 2/26/22 11:39 AM

@Entity(name = "user_answer")
public class UserAnswer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String answer;
    private String image_path;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "lesson_task_id")
    private LessonTask lessonTask;

    @Column(name = "status", columnDefinition = " default false")
    private Boolean status;
    @Column(name = "correct", columnDefinition = " default false")
    private Boolean correct;

}
