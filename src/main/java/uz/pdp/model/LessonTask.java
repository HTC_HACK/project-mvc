package uz.pdp.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data

//Asadbek Xalimjonov 2/25/22 12:07 PM

@Entity(name = "lesson_tasks")
public class LessonTask {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String description;
    private String file_path;

    @ManyToOne
    @JoinColumn(name = "lesson_id")
    private Lesson lesson;

}
