package uz.pdp.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Data

//Asadbek Xalimjonov 2/28/22 7:53 PM
@Entity(name = "course_tests")
public class CourseTest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String question;
    private String option_one;
    private String option_two;
    private String option_three;
    private String option_four;
    private Integer correct_option;

    @ManyToOne
    @JoinColumn(name = "course_id")
    private Course course;

}
