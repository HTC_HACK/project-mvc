package uz.pdp.controller.admin;


//Asadbek Xalimjonov 2/18/22 2:13 PM

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.controller.docs.PdfGenerator;
import uz.pdp.dao.AuthDao;
import uz.pdp.dao.CourseTestDao;
import uz.pdp.dtos.ChartDto;
import uz.pdp.service.AuthenticationService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/stats")
public class ChatStatsController {

    @Autowired
    AuthenticationService authenticationService;

    @Autowired
    AuthDao authDao;

    @Autowired
    CourseTestDao courseTestDao;

    @Autowired
    PdfGenerator pdfGenerator;

    @GetMapping
    public List<List> adminPage() {

        List<ChartDto> stats = authenticationService.getStats();
        List<List> stringList = new ArrayList<>();

        List test = new ArrayList<>();
        test.add("Users Enrollment");
        test.add("By Course");
        stringList.add(test);
        for (ChartDto stat : stats) {
            List stringList1 = new ArrayList<>();
            stringList1.add(stat.getLabel());
            stringList1.add(stat.getData());
            stringList.add(stringList1);
        }
        return stringList;

    }

    @GetMapping("/user/{id}")
    public List<List> getUserById(@PathVariable Integer id) {
        List<ChartDto> stats = authDao.getMentor(id);
        List<List> stringList = new ArrayList<>();

        List test = new ArrayList<>();
        test.add("Users Stats");
        test.add("By Role");
        stringList.add(test);
        for (ChartDto stat : stats) {
            List stringList1 = new ArrayList<>();
            stringList1.add(stat.getLabel());
            stringList1.add(stat.getData());
            stringList.add(stringList1);
        }
        return stringList;
    }

    @GetMapping("/user")
    public List<List> getUser() {
        List<ChartDto> stats = authenticationService.getUserStats();

        List<List> stringList = new ArrayList<>();

        List test = new ArrayList<>();
        test.add("Users Stats");
        test.add("By Role");
        stringList.add(test);
        for (ChartDto stat : stats) {
            List stringList1 = new ArrayList<>();
            stringList1.add(stat.getLabel());
            stringList1.add(stat.getData());
            stringList.add(stringList1);
        }
        return stringList;
    }


    public String changeColor(int i) {
        List<String> color = new ArrayList<>(Arrays.asList(
                "#34CE57",
                "#007BFF",
                "#28A745",
                "#DC3545",
                "#00938D",
                "#FD661F"
        ));
        if (i <= color.size()) return color.get(i);
        return color.get(0);
    }


}