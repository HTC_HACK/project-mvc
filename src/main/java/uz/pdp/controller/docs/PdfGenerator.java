package uz.pdp.controller.docs;


//Asadbek Xalimjonov 3/1/22 12:48 AM


import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.property.TextAlignment;
import org.springframework.stereotype.Repository;
import uz.pdp.model.User;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

@Repository
public class PdfGenerator {


    public File getCertificate(User user, Double result, String course) {
        File file = null;
        try (PdfWriter writer = new PdfWriter("src/main/resources/certificate.pdf")) {

            PdfDocument pdfDocument = new PdfDocument(writer);

            pdfDocument.setDefaultPageSize(PageSize.A4);
            pdfDocument.addNewPage();


            Document document = new Document(pdfDocument);
            Paragraph paragraph = new Paragraph("Certificate " + course.toUpperCase()).setFontSize(25).setTextAlignment(TextAlignment.CENTER);

            document.add(paragraph);

            Paragraph paragraph1 = new Paragraph(user.getUserDetail().getFirstName() + " " + user.getUserDetail().getLastName()).setFontSize(15).setTextAlignment(TextAlignment.CENTER);

            document.add(paragraph1);

            Paragraph paragraph2 = new Paragraph("Result " + result).setFontSize(15).setTextAlignment(TextAlignment.CENTER);

            document.add(paragraph2);


            document.close();
            file = new File("src/main/resources/certificate.pdf");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }


}
