package uz.pdp.controller.answer;


//Asadbek Xalimjonov 2/26/22 12:39 PM


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import uz.pdp.dtos.UserAnswerDto;
import uz.pdp.model.User;
import uz.pdp.service.UserAnswerService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/answer")
public class UserAnswerController {


    @Autowired
    UserAnswerService userAnswerService;

    @PostMapping("/save/answer/{lessonId}/task/{taskId}")
    public String saveAnswer(HttpServletRequest request, HttpSession session, @PathVariable Integer lessonId, @PathVariable Integer taskId, @RequestParam String answer) {

        User user = (User) session.getAttribute("authUser");

        if (user != null && !user.getIs_blocked() && user.getRole().getId() == 3) {
            userAnswerService.saveAnswer(user.getId(), taskId, answer);
            String referer = request.getHeader("Referer");
            return "redirect:" + referer;
        }
        return "redirect:/auth/login";
    }


    @GetMapping("/unchecked")
    public String getUncheckedAnswer(Model model, HttpSession session) {
        User user = (User) session.getAttribute("authUser");

        if (user != null && !user.getIs_blocked() && user.getRole().getId() == 2) {

            List<UserAnswerDto> uncheckedAnswer = userAnswerService.getUnCheckedAnswer(user.getId());
            model.addAttribute("activeAnswerClose", "open");
            model.addAttribute("activeAnswerSide", "active");
            model.addAttribute("activeUncheckedAnswer", "active");
            model.addAttribute("uncheckedAnswer", uncheckedAnswer);

            return "admin/answer/unchecked";
        }
        return "redirect:/auth/login";
    }

    @GetMapping("/checked")
    public String getCheckedAnswer(Model model, HttpSession session) {
        User user = (User) session.getAttribute("authUser");

        if (user != null && !user.getIs_blocked() && user.getRole().getId() == 2) {

            List<UserAnswerDto> checkedAnswer = userAnswerService.getCheckedAnswer(user.getId());
            model.addAttribute("activeAnswerClose", "open");
            model.addAttribute("activeAnswerSide", "active");
            model.addAttribute("activeCheckedAnswer", "active");
            model.addAttribute("checkedAnswer", checkedAnswer);

            return "admin/answer/answer";
        }
        return "redirect:/auth/login";
    }

    @GetMapping("/true/{answerId}")
    public String checkAnswerTrue(@PathVariable Integer answerId, HttpSession session, HttpServletRequest request) {
        User user = (User) session.getAttribute("authUser");

        if (user != null && !user.getIs_blocked() && user.getRole().getId() == 2) {

            userAnswerService.makeAnswerTrue(answerId);
            String referer = request.getHeader("Referer");
            return "redirect:" + referer;
        }
        return "redirect:/auth/login";
    }

    @GetMapping("/false/{answerId}")
    public String checkAnswerFalse(@PathVariable Integer answerId, HttpSession session, HttpServletRequest request) {
        User user = (User) session.getAttribute("authUser");

        if (user != null && !user.getIs_blocked() && user.getRole().getId() == 2) {

            userAnswerService.makeAnswerFalse(answerId);
            String referer = request.getHeader("Referer");
            return "redirect:" + referer;
        }
        return "redirect:/auth/login";
    }


}
