package uz.pdp.controller.task;


//Asadbek Xalimjonov 2/26/22 10:18 PM


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import uz.pdp.dao.TaskDao;
import uz.pdp.dtos.LessonTaskDto;
import uz.pdp.model.User;
import uz.pdp.service.LessonTaskService;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/lessons/tasks")
public class LessonTaskController {

    @Autowired
    TaskDao taskDao;

    @Autowired
    LessonTaskService lessonTaskService;

    @GetMapping("/{lessonId}")
    public String getAllTasks(@PathVariable Integer lessonId, HttpSession session, Model model) {
        User user = (User) session.getAttribute("authUser");
        if (user != null && user.getRole().getId() == 2) {
            model.addAttribute("lessonId", lessonId);
            model.addAttribute("tasks", lessonTaskService.getLessonTask(lessonId));
            return "admin/lesson/task";
        }
        return "redirect:/auth/login";
    }

    @GetMapping("/{lessonId}/create")
    public String createTask(Model model, HttpSession session, @PathVariable String lessonId) {
        User user = (User) session.getAttribute("authUser");
        if (user != null && user.getRole().getId() == 2) {
            model.addAttribute("lessonId", lessonId);
            return "admin/lesson/task-create";
        }
        return "redirect:/auth/login";
    }

    @PostMapping("/{lessonId}/create")
    public String saveTask(@RequestParam String description, HttpSession session, @PathVariable Integer lessonId) {
        User user = (User) session.getAttribute("authUser");
        if (user != null && user.getRole().getId() == 2) {
            LessonTaskDto lessonTaskDto = new LessonTaskDto();
            lessonTaskDto.setDescription(description);
            taskDao.save(lessonTaskDto, lessonId);
            return "redirect:/lessons/tasks/" + lessonId;
        }
        return "redirect:/auth/login";
    }


    @GetMapping("/{lessonId}/update/{taskId}")
    public String editTask(Model model, HttpSession session, @PathVariable Integer lessonId, @PathVariable Integer taskId) {
        User user = (User) session.getAttribute("authUser");
        if (user != null && user.getRole().getId() == 2) {
            model.addAttribute("task", taskDao.getLessonTaskDtoById(taskId));
            return "admin/lesson/task-edit";
        }
        return "redirect:/auth/login";
    }

    @PostMapping("/{lessonId}/update/{taskId}")
    public String updateTask(@RequestParam String description, HttpSession session, @PathVariable Integer lessonId, @PathVariable Integer taskId) {
        User user = (User) session.getAttribute("authUser");
        if (user != null && user.getRole().getId() == 2) {
            LessonTaskDto lessonTaskDto = new LessonTaskDto();
            lessonTaskDto.setDescription(description);
            lessonTaskDto.setId(taskId);
            taskDao.update(lessonTaskDto);
            return "redirect:/lessons/tasks/" + lessonId;
        }
        return "redirect:/auth/login";
    }


    @GetMapping("/{lessonId}/delete/{taskId}")
    public String deleteTask(HttpSession session, @PathVariable Integer lessonId, @PathVariable Integer taskId) {
        User user = (User) session.getAttribute("authUser");
        if (user != null && user.getRole().getId() == 2) {
            taskDao.delete(taskId);
            return "redirect:/lessons/tasks/" + lessonId;
        }
        return "redirect:/auth/login";
    }

}
