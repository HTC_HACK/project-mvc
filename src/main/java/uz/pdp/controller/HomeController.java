package uz.pdp.controller;


//Asadbek Xalimjonov 2/17/22 2:00 PM


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import uz.pdp.dao.AuthDao;
import uz.pdp.dtos.CourseDto;
import uz.pdp.model.Course;
import uz.pdp.service.CourseService;

import javax.imageio.ImageIO;
import javax.xml.bind.DatatypeConverter;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/")
public class HomeController {

    @Autowired
    CourseService courseService;
    @Autowired
    AuthDao authDao;


    @GetMapping
    public String homePage(Model model) {

        model.addAttribute("studentCount", authDao.getUsers());
        model.addAttribute("homePage", "active");
        return "index";
    }

    @GetMapping("/coursePage")
    public String getCoursePage(Model model) {
        List<CourseDto> courseDtoList = courseService.getUserCourses();
        List<CourseDto> courseDtoList1 = new ArrayList<>();
        for (CourseDto courseDto : courseDtoList) {
            if (courseDto.getImage_path() != null) {
                courseDto.setImage_path(b64(courseDto.getImage_path()));
            }
            courseDtoList1.add(courseDto);
        }

        model.addAttribute("courses", courseDtoList);
        model.addAttribute("coursePage", "active");
        return "course-list";
    }

    @GetMapping("/courseCategory")
    public String courseCategory(@RequestParam(required = false) String search, @RequestParam(required = false) Integer pageid, Model model) {
        int s = 0;
        if (pageid != null) {
            s=pageid;
        } else {
            s=1;
            pageid = 1;
        }
        int total = 2;

        if (pageid == 1) {
        } else {
            pageid = pageid - 1;
            pageid = pageid * total + 1;
        }
        int size = 0;
        List<CourseDto> allCourse = new ArrayList<>();
        if (search != null && search.length() > 0) {
            allCourse = courseService.getUserCourses(search);
            size = courseService.getUserCourses(search).size();
        } else {
            allCourse = courseService.getUserCourses(pageid,total);
            size = courseService.getAdminCourses().size();
        }

        List<CourseDto> courseDtoList1 = new ArrayList<>();
        for (CourseDto courseDto : allCourse) {
            if (courseDto.getImage_path() != null) {
                courseDto.setImage_path(b64(courseDto.getImage_path()));
            }
            courseDtoList1.add(courseDto);
        }

        model.addAttribute("courses", courseDtoList1);
        model.addAttribute("instructors",authDao.getMentorsList());
        model.addAttribute("size", size);
        model.addAttribute("pageid",s);
        model.addAttribute("courseCategory", "active");
        return "course-filter";
    }


    public String b64(String image) {
        try {
            String imgName = "" + image;
            BufferedImage bImage = ImageIO.read(new File(imgName));//give the path of an image
            ByteArrayOutputStream base = new ByteArrayOutputStream();
            ImageIO.write(bImage, "png", base);
            base.flush();
            byte[] imageInByteArray = base.toByteArray();
            base.close();
            return DatatypeConverter.printBase64Binary(imageInByteArray);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return image;
    }

}
