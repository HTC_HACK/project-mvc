package uz.pdp.dao;


//Asadbek Xalimjonov 2/18/22 6:02 PM


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import uz.pdp.dtos.ChartDto;
import uz.pdp.dtos.UserDto;
import uz.pdp.model.Role;
import uz.pdp.model.User;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class AuthDao {

    @Autowired
    private SessionFactory sessionFactory;
    @Autowired
    JdbcTemplate template;

    public String registerUser(User user) {
        Session currentSession = sessionFactory.getCurrentSession();
        user.setRole(getRole());
        user.setIs_blocked(false);
        currentSession.saveOrUpdate(user);
        return "Success";
    }

    public Role getRole() {
        Session currentSession = sessionFactory.getCurrentSession();
        Role role = currentSession.get(Role.class, 3);
        return role;
    }


    public User findByEmail(String email) {
        Session currentSession = sessionFactory.getCurrentSession();
        Query query = currentSession.createQuery("from users where email = '" + email + "'");
        User user = (User) query.uniqueResult();
        return user;
    }

    public User findByEmailPassword(String email, String password) {
        Session currentSession = sessionFactory.getCurrentSession();
        Query query = currentSession.createQuery("from users where email = '" + email + "' and password='" + password + "'");
        User user = (User) query.uniqueResult();
        return user;
    }

    public void updateUser(User updateUser) {

        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("update users set userDetail.firstName =:firstName,userDetail.lastName=:lastName,userDetail.image_path=:image_path where id = :id ");
        query.setString("firstName", updateUser.getUserDetail().getFirstName());
        query.setString("lastName", updateUser.getUserDetail().getLastName());
        query.setString("image_path", updateUser.getUserDetail().getImage_path());
        query.setInteger("id", updateUser.getId());
        query.executeUpdate();


    }

    public void updateUserImg(User updateUser) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("update users set userDetail.firstName =:firstName,userDetail.lastName=:lastName where id = :id ");
        query.setString("firstName", updateUser.getUserDetail().getFirstName());
        query.setString("lastName", updateUser.getUserDetail().getLastName());
        query.setInteger("id", updateUser.getId());
        query.executeUpdate();
    }

    public void addAccount(User updateUser) {
        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.saveOrUpdate(updateUser);
    }


    public void blockUser(int id) {
        String sqlQuery = "update users set is_blocked = true where id = " + id;
        template.update(sqlQuery);
    }

    public void unblockUser(int id) {
        String sqlQuery = "update users set is_blocked = false where id = " + id;
        template.update(sqlQuery);
    }

    public List<User> getAllUser() {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<User> cq = cb.createQuery(User.class);
        Root<User> root = cq.from(User.class);
        cq.select(root);
        Query query = session.createQuery(cq);
        return query.getResultList();
    }

    public List<User> getAllMentor() {
        Session currentSession = sessionFactory.getCurrentSession();
        Query query = currentSession.createQuery("from users where role.id=2");
        return query.getResultList();
    }

    public User findById(Integer s) {
        Integer id = Integer.valueOf(s);
        Session currentSession = sessionFactory.getCurrentSession();
        return currentSession.get(User.class, id);
    }

    public void registerUserAdmin(User updateUser) {
        Session currentSession = sessionFactory.getCurrentSession();
        updateUser.setRole(getRoleUser(updateUser.getRole().getId()));
        updateUser.setIs_blocked(false);
        currentSession.saveOrUpdate(updateUser);
    }

    public Role getRoleUser(Integer id) {
        Session currentSession = sessionFactory.getCurrentSession();
        Role role = currentSession.get(Role.class, id);
        return role;
    }

    public List<UserDto> search(String search) {
        String queryStr = "select u.id as id, u.email as email,r.name as role,u.firstname as firstname, u.is_blocked as is_blocked, u.image_path as image_path, u.lastname as lastname  from users u join roles r on u.role_id = r.id  where firstname ilike '%" + search + "%' or lastname ilike '%" + search + "%' ";
        return template.query(queryStr, (rs, row) -> {
            UserDto e = new UserDto();
            e.setId(rs.getInt("id"));
            e.setEmail(rs.getString("email"));
            e.setRole(rs.getString("role"));
            e.setFirstname(rs.getString("firstname"));
            e.setLastname(rs.getString("lastname"));
            e.setImage_path(rs.getString("image_path"));
            e.setIs_blocked(rs.getBoolean("is_blocked"));
            return e;
        });
    }

    public List<UserDto> getPageUser(Integer pageid, int total) {
        String queryStr = "select u.id as id, u.email as email,r.name as role,u.firstname as firstname, u.is_blocked as is_blocked, u.image_path as image_path, u.lastname as lastname  from users u join roles r on u.role_id = r.id limit " + total + " offset " + (pageid - 1) + ";";
        return template.query(queryStr, (rs, row) -> {
            UserDto e = new UserDto();
            e.setId(rs.getInt("id"));
            e.setEmail(rs.getString("email"));
            e.setRole(rs.getString("role"));
            e.setFirstname(rs.getString("firstname"));
            e.setLastname(rs.getString("lastname"));
            e.setImage_path(rs.getString("image_path"));
            e.setIs_blocked(rs.getBoolean("is_blocked"));
            return e;
        });
    }

    public List<UserDto> getALlByMentorId(Integer id) {
        String queryStr = "select distinct u.id       as id,\n" + "       email      as email,\n" + "       name       as role,\n" + "       firstname  as firstname,\n" + "       image_path as image_path,\n" + "       lastname   as lastname\n" + "from course_user cu\n" + "         join users u\n" + "              on user_id = u.id\n" + "         join roles r on r.id = role_id\n" + "         join course_author ca on cu.course_id = ca.course_id\n" + "where ca.author_id = '" + id + "';";
        return template.query(queryStr, (rs, row) -> {
            UserDto e = new UserDto();
            e.setId(rs.getInt("id"));
            e.setEmail(rs.getString("email"));
            e.setRole(rs.getString("role"));
            e.setFirstname(rs.getString("firstname"));
            e.setLastname(rs.getString("lastname"));
            e.setImage_path(rs.getString("image_path"));
            return e;
        });
    }

    public List<ChartDto> getStats() {

        String query = "select courses.name as label,\n" +
                "       cte.amount\n" +
                "           as data\n" +
                "from courses\n" +
                "    join (select course_user.course_id, count(course_user.*) as amount\n" +
                "    from course_user\n" +
                "    group by course_user.course_id) cte on courses.id = cte.course_id\n" +
                "group by courses.name,cte.amount order by data desc;";

        return template.query(query, (rs, row) -> {
            ChartDto e = new ChartDto();
            e.setLabel(rs.getString("label"));
            e.setData(rs.getDouble("data"));
            return e;
        });

    }


    public Integer getMentors() {
        String queryStr = "select count(*) from users  where role_id=2";
        return template.queryForObject(queryStr, (rs, row) -> {
            Integer e = rs.getInt(1);
            return e;
        });
    }

    public Integer getUsers() {
        String queryStr = "select count(*) from users  where role_id=3";
        return template.queryForObject(queryStr, (rs, row) -> {
            Integer e = rs.getInt(1);
            return e;
        });
    }

    public List<ChartDto> getUserStats() {
        String query = " select r.name as label ,\n" +
                "        count(*) as data\n" +
                "\n" +
                "        from users\n" +
                "        join roles r on r.id = users.role_id\n" +
                "        group by r.name;";

        return template.query(query, (rs, row) -> {
            ChartDto e = new ChartDto();
            e.setLabel(rs.getString("label"));
            e.setData(rs.getDouble("data"));
            return e;
        });
    }

    public List<ChartDto> getMentor(Integer id) {
        String query = "select courses.name as label,\n" +

                "      cte.amount" +
                "                    as data\n" +
                "from courses\n" +
                "         join (select course_user.course_id, count(course_user.*) as amount\n" +
                "\n" +
                "               from course_user\n" +
                "                        join course_author ca on ca.course_id = course_user.course_id\n" +
                "               where ca.author_id = " + id + "\n" +
                "               group by course_user.course_id) cte on courses.id = cte.course_id\n" +
                "group by courses.name,cte.amount\n" +
                "order by data desc;";

        return template.query(query, (rs, row) -> {
            ChartDto e = new ChartDto();
            e.setLabel(rs.getString("label"));
            e.setData(rs.getDouble("data"));
            return e;
        });
    }

    public List<UserDto> getMentorsList() {
        String queryStr = "select * from users  where role_id=2";
        return template.query(queryStr, (rs, row) -> {
            UserDto e = new UserDto();
            e.setId(rs.getInt("id"));
            e.setFirstname(rs.getString("firstname"));
            e.setLastname(rs.getString("lastname"));
            return e;
        });
    }
}
