package uz.pdp.dao;


//Asadbek Xalimjonov 2/28/22 8:09 PM


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import uz.pdp.dtos.ResultDto;
import uz.pdp.model.CourseTest;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

@Repository
public class CourseTestDao {


    @Autowired
    JdbcTemplate template;

    public List<CourseTest> subjectId(Integer courseId) {
        String queryStr = "select *\n" + "from course_tests\n" + "where course_id=" + courseId + ";";
        return template.query(queryStr, (rs, row) -> {
            CourseTest e = new CourseTest();
            e.setId(rs.getInt("id"));
            e.setQuestion(rs.getString("question"));
            e.setOption_one(rs.getString("option_one"));
            e.setOption_two(rs.getString("option_two"));
            e.setOption_three(rs.getString("option_three"));
            e.setOption_four(rs.getString("option_four"));
            e.setCorrect_option(rs.getInt("correct_option"));
            return e;
        });
    }

    public void calculateResult(Map<String, String[]> parameterMap, Integer userId, Integer courseId) {

        int result = 0;
        int total = 0;

        for (CourseTest courseTest : subjectId(courseId)) {
            for (String s : parameterMap.keySet()) {
                if (!s.equals("subjects")) {
                    int num = Integer.parseInt(s);
                    if (num == courseTest.getId()) {
                        String[] strings = parameterMap.get(s);
                        String string = strings[1];
                        int num2 = Integer.parseInt(string);

                        System.out.println(strings);
                        if (courseTest.getCorrect_option() == num2) {
                            result += 1;
                        }
                        total += 1;
                    }
                }
            }
        }

        int totalResult = result * 100 / total;

        String sqlQuery = "insert into results(result, user_id, course_id) values('" + totalResult + "', " + userId + ", " + courseId + ");";
        template.update(sqlQuery);

    }

    public Double getResult(Integer id, Integer id1) {
        try {
            String queryStr = "select result from results where course_id=" + id + " and user_id=" + id1 + " ;";
            return template.queryForObject(queryStr, (rs, row) -> {
                Double e = rs.getDouble(1);
                return e;
            });
        } catch (Exception e) {
        }
        return null;
    }

    public String certificateDate(Integer course_id, Integer user_id) {
        String queryStr = "select created_at from results where course_id=" + course_id + " and user_id=" + user_id + " ;";
        LocalDateTime localDateTime = template.queryForObject(queryStr, (rs, row) -> {
            LocalDateTime dateTime = rs.getTimestamp(1).toLocalDateTime();
            return dateTime;
        });
        return localDateTime.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
    }

    public List<ResultDto> getAllResults() {

        String queryStr = "select " +
                "concat(u.firstname,' ',u.lastname) as fullName," +
                "results.result as score," +
                "c.name as course\n" +
                "from results\n" +
                "join users u on u.id = results.user_id\n" +
                "join courses c on c.id = results.course_id";

        return template.query(queryStr, (rs, row) -> {
            ResultDto e = new ResultDto();
            e.setFullName(rs.getString("fullName"));
            e.setCourse(rs.getString("course"));
            e.setScore(rs.getDouble("score"));
            return e;
        });

    }

    public List<ResultDto> getAllResultsMentor(Integer mentorId) {

        String queryStr = "select concat(u.firstname,' ',u.lastname) as fullName,results.result as score,c.name as course\n" +
                "from results\n" +
                "join users u on u.id = results.user_id\n" +
                "join courses c on c.id = results.course_id\n" +
                "join course_author ca on c.id = ca.course_id\n" +
                "where ca.author_id=" + mentorId;

        return template.query(queryStr, (rs, row) -> {
            ResultDto e = new ResultDto();
            e.setFullName(rs.getString("fullName"));
            e.setCourse(rs.getString("course"));
            e.setScore(rs.getDouble("score"));
            return e;
        });

    }
}
