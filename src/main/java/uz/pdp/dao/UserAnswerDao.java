package uz.pdp.dao;


//Asadbek Xalimjonov 2/26/22 12:48 PM


import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import uz.pdp.dtos.UserAnswerDto;

import java.util.List;

@Repository
public class UserAnswerDao {

    @Autowired
    private SessionFactory sessionFactory;
    @Autowired
    JdbcTemplate template;


    public void saveAnswer(Integer userId, Integer taskId, String answer) {

        String sql = "insert into user_answer(answer,user_id,lesson_task_id) values('" + answer + "','" + userId + "','" + taskId + "')";
        template.update(sql);

    }

    public List<UserAnswerDto> getUnCheckedAnswer(Integer userid) {

        String queryStr = "select user_answer.*,\n" +
                "       lt.description,\n" +
                "       concat(u.firstname,' ',u.lastname) as fullName,\n" +
                "       l.name as lessonName\n" +
                "from user_answer\n" +
                "         join lesson_tasks lt on lt.id = user_answer.lesson_task_id\n" +
                "         join lessons l on l.id = lt.lesson_id\n" +
                "         join modules m on m.id = l.module_id\n" +
                "         join courses c on c.id = m.course_id\n" +
                "         join course_author ca on c.id = ca.course_id\n" +
                "join users u on u.id = user_answer.user_id\n" +
                "where status = false\n" +
                "  and ca.author_id = " + userid + " ;";

        return template.query(queryStr, (rs, row) -> {
            UserAnswerDto userAnswerDto = new UserAnswerDto();
            userAnswerDto.setId(rs.getInt("id"));
            userAnswerDto.setAnswer(rs.getString("answer"));
            userAnswerDto.setStatus(rs.getBoolean("status"));
            userAnswerDto.setCorrect(rs.getBoolean("correct"));
            userAnswerDto.setTaskDescription(rs.getString("description"));
            userAnswerDto.setFullName(rs.getString("fullName"));
            userAnswerDto.setLessonName(rs.getString("lessonName"));
            return userAnswerDto;
        });

    }

    public List<UserAnswerDto> getCheckedAnswer(Integer id) {

        String queryStr = "select user_answer.*,\n" +
                "       lt.description,\n" +
                "       concat(u.firstname,' ',u.lastname) as fullName,\n" +
                "       l.name as lessonName\n" +
                "from user_answer\n" +
                "         join lesson_tasks lt on lt.id = user_answer.lesson_task_id\n" +
                "         join lessons l on l.id = lt.lesson_id\n" +
                "         join modules m on m.id = l.module_id\n" +
                "         join courses c on c.id = m.course_id\n" +
                "         join course_author ca on c.id = ca.course_id\n" +
                "join users u on u.id = user_answer.user_id\n" +
                "where status = true\n" +
                "  and ca.author_id = " + id + " ;";

        return template.query(queryStr, (rs, row) -> {
            UserAnswerDto userAnswerDto = new UserAnswerDto();
            userAnswerDto.setId(rs.getInt("id"));
            userAnswerDto.setAnswer(rs.getString("answer"));
            userAnswerDto.setStatus(rs.getBoolean("status"));
            userAnswerDto.setCorrect(rs.getBoolean("correct"));
            userAnswerDto.setTaskDescription(rs.getString("description"));
            userAnswerDto.setFullName(rs.getString("fullName"));
            userAnswerDto.setLessonName(rs.getString("lessonName"));
            return userAnswerDto;
        });
    }

    public void makeAnswerTrue(Integer answerId) {
        String sql = "update user_answer set status=true,correct=true where id=" + answerId;
        template.update(sql);
    }

    public void makeAnswerFalse(Integer answerId) {
        String sql = "update user_answer set status=true where id=" + answerId;
        template.update(sql);
    }
}
