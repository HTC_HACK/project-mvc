package uz.pdp.dao;


//Asadbek Xalimjonov 2/26/22 12:16 PM


import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import uz.pdp.dtos.LessonTaskDto;
import uz.pdp.dtos.UserAnswerDto;

import java.util.List;

@Repository
public class LessonTaskDao {

    @Autowired
    private SessionFactory sessionFactory;
    @Autowired
    JdbcTemplate template;


    public List<LessonTaskDto> getTaskByLessonId(Integer lessonId, Integer userId) {
        String queryStr = "select *\n" +
                "from lesson_tasks\n" +
                "where lesson_id=" + lessonId + " order by lesson_tasks.id asc;";
        return template.query(queryStr, (rs, row) -> {
            LessonTaskDto lessonTask = new LessonTaskDto();
            lessonTask.setId(rs.getInt("id"));
            lessonTask.setDescription(rs.getString("description"));
            lessonTask.setFile_path(rs.getString("file_path"));
            if (getTaskTrue(rs.getInt("id"), userId) > 0) lessonTask.setCorrect(true);
            lessonTask.setUserAnswers(getTaskWithAnswer(rs.getInt("id"), userId));
            return lessonTask;
        });
    }

    public Integer getTaskTrue(Integer taskId, Integer userId) {
        String queryStr = "select count(*)\n" +
                "from user_answer\n" +
                "where user_id=" + userId + " and lesson_task_id=" + taskId + " and correct=true";

        return template.queryForObject(queryStr, (rs, row) -> {
            Integer a = rs.getInt(1);
            return a;
        });
    }

    public List<UserAnswerDto> getTaskWithAnswer(Integer lessonTaskId, Integer userId) {
        String queryStr = "select user_answer.*\n" +
                "from user_answer\n" +
                "where user_id=" + userId + " and lesson_task_id=" + lessonTaskId + " order by user_answer.id desc";

        return template.query(queryStr, (rs, row) -> {
            UserAnswerDto lessonTask = new UserAnswerDto();
            lessonTask.setId(rs.getInt("id"));
            lessonTask.setAnswer(rs.getString("answer"));
            lessonTask.setStatus(rs.getBoolean("status"));
            lessonTask.setCorrect(rs.getBoolean("correct"));
            return lessonTask;
        });

    }
}
