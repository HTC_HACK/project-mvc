package uz.pdp.dao;


//Asadbek Xalimjonov 2/26/22 9:54 PM


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import uz.pdp.dtos.LessonTaskDto;

import java.util.List;

@Repository
public class TaskDao {

    @Autowired
    JdbcTemplate template;


    public int save(LessonTaskDto task, int id) {
        String sql = "insert into lesson_tasks(description, lesson_id) values" + "('" + task.getDescription() + "', " + id + ");";
        return template.update(sql);
    }

    public int delete(int id) {
        String sql = "delete from lesson_tasks where id=" + id + "";
        return template.update(sql);
    }

    public int update(LessonTaskDto task) {
        String sql = "update lesson_tasks set description='" + task.getDescription() + "' where id=" + task.getId();
        return template.update(sql);
    }


    public LessonTaskDto getLessonTaskDtoById(int id) {
        String queryStr = "select * from lesson_tasks where id = ?";
        return template.queryForObject(queryStr, new Object[]{id}, new BeanPropertyRowMapper<LessonTaskDto>(LessonTaskDto.class));
    }

    public List<LessonTaskDto> getLessonTaskDtoByLessonId(int id) {
        String queryStr = "select * from lesson_tasks where lesson_id = " + id + ";";


        List<LessonTaskDto> taskList = template.query(queryStr, (rs, row) -> {
            LessonTaskDto task = new LessonTaskDto();
            task.setId(rs.getInt(1));
            task.setDescription(rs.getString(2));
            task.setLessonId(rs.getInt(3));
            return task;
        });
        return taskList;
    }

}
